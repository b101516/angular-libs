import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { CfOrderLibModule } from 'projects/cf-order-lib/src/lib';
import { OrdersComponent } from './orders.component';

@NgModule({
  declarations: [
    OrdersComponent
  ],
  imports: [
    CommonModule,

    CfOrderLibModule,

    RouterModule.forChild([{
      path: '',
      component: OrdersComponent
    }])
  ],
  
})
export class OrdersModule { }
