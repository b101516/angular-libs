import { NgModule } from '@angular/core';

import { CfDisplayOrderLibComponent } from './cf-display-order-lib.component';

@NgModule({
  declarations: [
    CfDisplayOrderLibComponent
  ],
  imports: [
  ],
  exports: [
    CfDisplayOrderLibComponent
  ]
})
export class CfDisplayOrderLibModule { }
