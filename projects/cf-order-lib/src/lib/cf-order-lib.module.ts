import { NgModule } from '@angular/core';
import { CfDisplayOrderLibModule } from 'projects/cf-display-order-lib/src/lib';

import { CfOrderLibComponent } from './cf-order-lib.component';

@NgModule({
  declarations: [
    CfOrderLibComponent
  ],
  imports: [
    CfDisplayOrderLibModule
  ],
  exports: [
    CfOrderLibComponent
  ]
})
export class CfOrderLibModule { }
